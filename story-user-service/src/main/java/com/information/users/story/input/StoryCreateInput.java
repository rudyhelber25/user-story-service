package com.information.users.story.input;

import lombok.Data;

/**
 * @author Rudy Choque
 */
@Data
public class StoryCreateInput {


    private Long userId;

    private String name;

    private String email;

    private Integer phone;




}
