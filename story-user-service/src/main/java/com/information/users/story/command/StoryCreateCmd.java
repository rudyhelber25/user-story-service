package com.information.users.story.command;

import com.information.users.story.input.StoryCreateInput;
import com.information.users.story.model.domain.Detail;
import com.information.users.story.model.domain.Story;
import com.information.users.story.model.repository.DetailRepository;
import com.information.users.story.model.repository.StoryRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Rudy Choque
 */
//se convierte en bean para el manejo de transacciones ya tiene transacciones and bean
@SynchronousExecution
public class StoryCreateCmd implements BusinessLogicCommand {

    @Setter
    private StoryCreateInput input;

    @Getter
    private Story story;

    @Autowired
    private DetailRepository detailRepository;

    @Autowired
    private StoryRepository contactRepository;

    @Override
    public void execute() {
        Detail detail = detailRepository.save(composeDetailInstance());
        story = contactRepository.save(composeContactInstance(detail));
    }

    private Story composeContactInstance(Detail detail) {
        Story instance = new Story();
        instance.setUserId(input.getUserId());
        instance.setName(input.getName());
        instance.setEmail(input.getEmail());
        instance.setPhone(input.getPhone());
        instance.setDetail(detail);

        return instance;

    }

    private Detail composeDetailInstance() {
        Detail instance = new Detail();
        instance.setInformation("OnlyCalls");
        return instance;
    }


}
