package com.information.users.story.model.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static com.information.users.story.model.domain.Constants.DetailTable;

/**
 * @author Santiago Mamani
 */
@Data
@Entity
@Table(name = DetailTable.NAME)
public class Detail implements Serializable {
    //serializable comprimi objeto para enviar a BD
    @Id
    @Column(name = DetailTable.Id.NAME, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = DetailTable.Information.NAME, length = DetailTable.Information.LENGHT, nullable = false)
    private String information;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = DetailTable.CreatedDate.NAME, nullable = false, updatable = false)
    private Date createdDate;

    //antes de persistir va hacer estas cosas
    @PrePersist
    void onPrePersist() {
        this.createdDate = new Date();
    }
}
