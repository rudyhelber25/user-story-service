package com.information.users.story.model.repository;

import com.information.users.story.model.domain.Detail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Santiago Mamani
 */
public interface DetailRepository extends JpaRepository<Detail, Long> {
}
