package com.information.users.story.controller.system;


import com.information.users.story.command.StoryCreateCmd;
import com.information.users.story.controller.Constants;
import com.information.users.story.input.StoryCreateInput;
import com.information.users.story.model.domain.Story;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;


/**
 * @author Rudy Choque
 */
@Api(
        tags = Constants.StoryTag.NAME,
        description = Constants.StoryTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.SYSTEM_STORY)
@RequestScope
public class SystemStoryCreateController {

    @Autowired
    private StoryCreateCmd storyCreateCmd;

    @ApiOperation(
            value = "Create a story {Implementation is pending}"
    )
    @RequestMapping(method = RequestMethod.POST)
    public Story createStory(@RequestBody StoryCreateInput input) {
        // throw new UnsupportedOperationException("Implementation is pending");
        storyCreateCmd.setInput(input);
        storyCreateCmd.execute();
        return storyCreateCmd.getStory();
    }
}
