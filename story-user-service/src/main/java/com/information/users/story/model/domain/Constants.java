package com.information.users.story.model.domain;

/**
 * @author Santiago Mamani
 */
public final class Constants {

    private Constants() {
    }

    public static class StoryTable {
        public static final String NAME = "story_table";

        public static class Id {
            public static final String NAME = "storyid";
        }

        public static class UserId {
            public static final String NAME = "userid";
        }

        public static class Name {
            public static final String NAME = "name";

            public static final int LENGTH = 200;
        }

        public static class Email {
            public static final String NAME = "email";

            public static final int LENGTH = 100;
        }

        public static class Phone {
            public static final String NAME = "phone";

            public static final int LENGTH = 8;
        }

        public static class CreatedDate {
            public static final String NAME = "createddate";
        }

        public static class DetailId {
            public static final String NAME = "detailid";
        }
    }

    public static class DetailTable {

        public static final String NAME = "detail_table";

        public static class Id {
            public static final String NAME = "detailid";
        }

        public static class Information {
            public static final String NAME = "information";
            public static final int LENGHT = 200;
        }

        public static class CreatedDate {
            public static final String NAME = "createddate";
        }
    }
}
