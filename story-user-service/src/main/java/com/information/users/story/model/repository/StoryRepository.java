package com.information.users.story.model.repository;

import com.information.users.story.model.domain.Story;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Santiago Mamani
 */
public interface StoryRepository extends JpaRepository<Story, Long> {
}
