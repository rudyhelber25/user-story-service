package com.information.users.story.model.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static com.information.users.story.model.domain.Constants.StoryTable;
import static com.information.users.story.model.domain.Constants.DetailTable;

/**
 * @author Santiago Mamani
 */
@Data
@Entity  //genera getter and setter minimizador de codigo
@Table(name = StoryTable.NAME)
public class Story implements Serializable {

    @Id
    @Column(name = StoryTable.Id.NAME, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = StoryTable.UserId.NAME)
    private Long userId;

    @Column(name = StoryTable.Name.NAME, length = StoryTable.Name.LENGTH, nullable = false)
    private String name;

    @Column(name = StoryTable.Email.NAME, length = StoryTable.Email.LENGTH, nullable = false)
    private String email;

    @Column(name = StoryTable.Phone.NAME, length = StoryTable.Phone.LENGTH, nullable = false)
    private Integer phone;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = StoryTable.CreatedDate.NAME, nullable = false, updatable = false)
    private Date createdDate;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = StoryTable.DetailId.NAME, referencedColumnName = DetailTable.Id.NAME, nullable = false)
    private Detail detail;

    @PrePersist
    void onPrePersist() {
        this.createdDate = new Date();
    }
}
